%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.05.2021

%% ====================================================================
%% Types
%% ====================================================================

-record(event_hash, {
    key :: {any | atom() | binary(), any | binary()},
    value,
    hash :: integer()
}).

-record(event_map, {
    key :: {any | atom() | binary(), any | binary()},
    list = [] :: [binary()]
}).

%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(ClassesCN, <<"classes">>).
-define(SubscriptionsCN, <<"subscriptions">>).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(PLATFORMLIB, platformlib).

-define(MsvcDms, <<"dms">>).

-define(APP, subscr).
-define(MsvcType, <<"subscr">>).

-define(SUPV, subscr_supv).
-define(CFG, subscr_config).

-define(FixturerCallback, subscr_fixturer_callback).
-define(FixturerLeaderName, 'subscr_fixturer_leader_srv').

-define(FixturerClasses, subscr_fixture_coll_classes).

-define(VALIDATOR, subscr_validator_srv).
-define(ValidatorSubscriptions, subscr_validator_coll_subscriptions).

-define(DomainTreeCallback, subscr_domaintree_callback).

-define(DSUPV, subscr_domain_supv).
-define(DSRV, subscr_domain_srv).
-define(DSearchSrv, subscr_domain_search_srv).
-define(DSearchUtils, subscr_domain_search).
-define(DU, subscr_domain_utils).
-define(DModifySrv, subscr_domain_modify_srv).
-define(DModifyUtils, subscr_domain_modify).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).
-define(BLstore, basiclib_store).

%% ------
%% From platformlib
%% ------
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(LeaderSupv, platformlib_leader_app_supv).
-define(LeaderU, platformlib_leader_app_utils).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CRUD, platformlib_dms_crud).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_FIXTURER_SUPV, platformlib_dms_fixturer_leader_supv).

-define(DTREE_SUPV, platformlib_domaintree_supv).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {subscr,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).

%% ====================================================================
%% Define other
%% ====================================================================
