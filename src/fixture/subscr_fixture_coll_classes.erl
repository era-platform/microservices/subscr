%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Controlled classes descriptions (for dms collection 'classes')

-module(subscr_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([subscriptions/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "subscriptions"
%% return fixtured entity of controlled class
%% -------------------------------------
subscriptions(Domain) ->
    #{
        <<"id">> => ?BU:to_guid(<<"085476d3-0179-b3f1-3ffe-7cd30a921f58">>),
        <<"classname">> => ?SubscriptionsCN,
        <<"name">> => <<"Subscriptions">>,
        <<"description">> => <<"General platform collection. Subscriptions for event notifications.">>,
        <<"storage_mode">> => <<"runtime">>,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => <<"subscr:validator">>,
            <<"appserver">> => ?GN_NAMING:get_globalname(?MsvcType,Domain),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => true,
            <<"notify_integrity_timeout">> => 5000,
            <<"max_limit">> => 1000,
            <<"max_size">> => 1000000,
            <<"expires_mode">> => <<"modify">>,
            <<"expires_ttl_property">> => <<"expires">>,
            <<"expires_ts_property">> => <<"timestamp">>,
            <<"lookup_properties">> => [<<"sid">>]
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"sid">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"expires">>,
                <<"data_type">> => <<"integer">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"timestamp">>,
                <<"data_type">> => <<"long">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"subscriber">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"events">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => true
            },
            #{
                <<"name">> => <<"objects">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => true
            },
            #{
                <<"name">> => <<"exargs">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================