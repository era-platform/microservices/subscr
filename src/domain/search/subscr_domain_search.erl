%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.05.2021
%%% @doc

-module(subscr_domain_search).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([search/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------------
-spec search(Args :: list(), % [KeyList] | [KeyList,Opts]
%                            %   when KeyList:: [EventName::binary()] | [{EventName::binary(),ObjectName::binary()}]] |
                             %        Opts:: [Opt], Opt::'no_wildcards' | 'local'
             EtsMap :: ets:tab(), EtsHash :: ets:tab()) -> {ok,[{Key::binary(),Data::map()}]}.
%% ---------------------------------------------
search([KeyList], EtsMap, EtsHash) when is_list(KeyList) ->
    do_search('wildcards',KeyList,[], EtsMap, EtsHash);
%%
search([KeyList, Opts], EtsMap, EtsHash) when is_list(KeyList) ->
    WildcardsKey = case lists:member('no_wildcards',Opts) of
                       true -> 'no_wildcards';
                       false -> 'wildcards'
                   end,
    do_search(WildcardsKey,KeyList,Opts,EtsMap,EtsHash);
%%
search(_,_,_) -> {ok,[]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
do_search(no_wildcards, KeyList, Opts, EtsMap, EtsHash) ->
    SearchList = lists:foldl(fun({Ev,Obj},Acc) -> [{Ev,Obj}|Acc] end, [], KeyList),
    F = fun(El, Acc) -> case ets:lookup(EtsMap, El) of
                            [] -> Acc;
                            [#event_map{list=L}] -> L++Acc
                        end end,
    OnlyLocal = lists:member('local',Opts),
    case lists:usort(lists:foldl(F, [], SearchList)) of
        [] -> {ok,[]};
        Keys when OnlyLocal -> {ok,filter_local(get_subscriptions(Keys,EtsHash))};
        Keys -> {ok,get_subscriptions(Keys,EtsHash)}
    end;
%%
do_search(wildcards, KeyList, Opts, EtsMap, EtsHash) ->
    SearchList = lists:foldl(fun({Ev,Obj},Acc) ->
                                       % parametrized by objects events
                                       EvStar = make_event_mask(Ev),
                                       [{Ev,Obj},
                                        {EvStar,Obj},
                                        {any,Obj},
                                        {Ev,any},
                                        {EvStar,any} | Acc];
                                (Ev,Acc) when is_binary(Ev) ->
                                       % non-parametrized by objects events
                                       EvStar = make_event_mask(Ev),
                                       [{Ev,any},
                                        {EvStar,any} | Acc]
                             end, [], KeyList)++[{any,any}],
    F = fun(El, Acc) -> case ets:lookup(EtsMap, El) of
                            [] -> Acc;
                            [#event_map{list=L}] -> L++Acc
                        end end,
    OnlyLocal = lists:member('local',Opts),
    case lists:usort(lists:foldl(F, [], SearchList)) of
        [] -> {ok,[]};
        Keys when OnlyLocal -> {ok,filter_local(get_subscriptions(Keys,EtsHash))};
        Keys -> {ok,get_subscriptions(Keys,EtsHash)}
    end.

%% @private
make_event_mask(Ev) ->
    [EvClass,_] = binary:split(Ev, <<".">>),
    <<EvClass/binary,".*">>.

%% @private
get_subscriptions(Keys,EtsHash) ->
    lists:foldl(fun(Key,Acc) ->
                    case ets:lookup(EtsHash,Key) of
                        [#event_hash{value=Item}] -> [Item|Acc];
                        [] -> Acc
                    end end, [], Keys).

%% @private
filter_local(Items) ->
    CurSite = ?PCFG:get_current_site(),
    lists:filter(fun(Item) ->
                        Subscriber = maps:get(<<"subscriber">>,Item),
                        case maps:get(<<"site">>,Subscriber,undefined) of
                            undefined -> true;
                            CurSite -> true;
                            _ -> false
                        end end, Items).