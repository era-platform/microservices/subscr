%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.05.2021
%%% @doc Modifier gen_server for subscr domain service

-module(subscr_domain_modify_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([build_entity/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    ref :: reference(),
    ets_data :: ets:tab(),
    ets_map :: ets:tab(),
    ets_hash :: ets:tab()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% helps to build entity everywhere
build_entity(Args) ->
    do_build_entity(Args).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    SyncRef = ?BU:get_by_key(sync_ref,Opts),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    ref = Ref,
                    ets_data = ?BU:get_by_key(ets_data,Opts),
                    ets_map = ?BU:get_by_key(ets_map,Opts),
                    ets_hash = ?BU:get_by_key(ets_hash,Opts)},
    % ----
    self() ! {'init',Ref},
    % ----
    ?LOG('$info', "~ts. '~ts' modify srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% --------------
%% Put subscription
handle_cast({put,[Key,_Map],FunReply}, State) ->
    del([Key],FunReply,State);
handle_cast({put,[Key,_Map,TTL],FunReply}, State) when TTL=<0 ->
    del([Key],FunReply,State);
handle_cast({put,[Key,Map,TTL],FunReply}, State) ->
    put([Key,Map,TTL],FunReply,State);

%% --------------
%% Delete subscription
handle_cast({del,[Key],FunReply}, State) ->
    del([Key],FunReply,State);
handle_cast({del,[Key,_Map],FunReply}, State) ->
    del([Key],FunReply,State);

%% --------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init. sync to facade
handle_info({'init',Ref}, #dstate{ref=Ref,domain=Domain,sync_ref=SyncRef}=State) ->
    State1 = case ?BLstore:find_u({?APP,sync,Domain}) of
                 false -> State;
                 {_,{facade,SyncRef,Pid}} when is_pid(Pid) ->
                     case process_info(Pid,status) of
                         {status,_} -> Pid ! {register_srv,?MODULE,SyncRef,self()};
                         undefined -> erlang:send_after(1000,self(),{'init',Ref})
                     end,
                     State
             end,
    {noreply, State1};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%%
put([SID,_Map,TTL]=Args, FunReply, #dstate{domain=Domain,ets_data=EtsData,ets_map=EtsMap,ets_hash=EtsHash}=State) when is_map(_Map) ->
    EntityX = do_build_entity(Args),
    Reply = case validate(EntityX) of
                {ok,Entity} ->
                    Id = maps:get(<<"id">>,Entity),
                    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
                    PutReq = {crud,?SubscriptionsCN,{'replace',#{object => {'entity',Id,[]},
                                                                 content => Entity,
                                                                 qs => #{},
                                                                 initiator => {msvc,?MsvcType}}}},
                    case catch ?GLOBAL:gen_server_call(GlobalName, PutReq) of
                        {'EXIT',Reason} ->
                            ?LOG('$crash', "Call to DMS (replace subscription '~ts', '~ts', '~ts') crashed: ~n\t~120tp", [Domain, Id, SID, Reason]),
                            {error,{internal_error,<<"Operation crashed">>}};
                        {error,_}=Err ->
                            ?LOG('$error', "Call to DMS (replace subscription '~ts', '~ts', '~ts') error: ~n\t~120tp", [Domain, Id, SID, Err]),
                            Err;
                        {ok,Entity1} ->
                            % to be more guaranteed, store right now, this is second time, notify should be already in queue of facade
                            % but so clients should call the same node (if active-active) for the operations with the same subscription.
                            SID1 = maps:get(<<"sid">>,Entity1),
                            EntityShrink = maps:without([<<"expires">>,<<"timestamp">>],Entity1),
                            ets:insert(EtsData,{Id,EntityShrink}),
                            {ok,SID1} = ?DModifyUtils:on_put([SID1,EntityShrink,TTL],EtsMap,EtsHash)
                    end
            end,
    FunReply(Reply),
    {noreply, State}.

%%
del([SID], FunReply, #dstate{domain=Domain,ets_data=EtsData,ets_map=EtsMap,ets_hash=EtsHash}=State) ->
    case ets:lookup(EtsHash,SID) of
        [] -> FunReply(ok);
        [#event_hash{key=SID,value=EntityShrink}] ->
            Id = maps:get(<<"id">>,EntityShrink),
            DelReq = {crud,?SubscriptionsCN,{'delete',#{object => {'entity',Id,[]},
                                                        qs => #{},
                                                        initiator => {msvc,?MsvcType}}}},
            GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
            Reply = case catch ?GLOBAL:gen_server_call(GlobalName, DelReq) of
                        {'EXIT',Reason} ->
                            ?LOG('$crash', "Call to DMS (delete subscription '~ts', '~ts', '~ts') crashed: ~n\t~120tp", [Domain, Id, SID, Reason]),
                            {error,{internal_error,<<"Operation crashed">>}};
                        {error,{not_found,_}}=Err -> Err;
                        {error,_}=Err ->
                            ?LOG('$warning', "Call to DMS (delete subscription '~ts', '~ts', '~ts') returned: ~n\t~120tp", [Domain, Id, SID, Err]),
                            Err;
                        ok ->
                            ets:delete(EtsData,Id),
                            ok = ?DModifyUtils:on_delete([SID,EntityShrink],EtsMap,EtsHash)
                    end,
            FunReply(Reply)
    end,
    {noreply, State}.

%% @private
do_build_entity([SID,Map,TTL]) when is_map(Map) ->
    Id = case catch ?BU:to_guid(SID) of
             SID -> SID;
             _ -> ?BU:to_guid(?BU:md5(SID))
         end,
    #{<<"id">> => Id,
      <<"sid">> => SID,
      <<"expires">> => TTL,
      <<"subscriber">> => maps:get(<<"subscriber">>,Map,#{}),
      <<"events">> => maps:get(<<"events">>,Map,[]),
      <<"objects">> => maps:get(<<"objects">>,Map,[]),
      <<"exargs">> => maps:get(<<"exargs">>,Map,#{})
    }.

%% @private
validate(Entity) ->
    case maps:get(<<"expires">>,Entity) of
        TTL when not is_integer(TTL) -> {error,{invalid_params,<<"field=expires|Invalid 'expires'. Expected integer value">>}};
        TTL when TTL < 0 -> {error,{invalid_params,<<"field=expires|Invalid 'expires'. Expected non-negative value">>}};
        TTL when TTL > 86400 * 14 -> {error,{invalid_params,<<"field=expires|Invalid 'expires'. Expected integer value less than 1209600">>}};
        _ -> {ok,Entity}
    end.