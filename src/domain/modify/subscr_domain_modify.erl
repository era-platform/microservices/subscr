%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Storage index routines for subscriptions

-module(subscr_domain_modify).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([on_put/3,
         on_expire/3,
         on_delete/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------------
-spec on_put(Args :: list(), EtsMap :: ets:tab(), EtsHash :: ets:tab()) -> {ok,Key::binary()}.
%% ---------------------------------------------
on_put(Args, EtsMap, EtsHash) ->
    update_map(Args, EtsMap, EtsHash).

%% ---------------------------------------------
-spec on_expire(Args :: list(), EtsMap :: ets:tab(), EtsHash :: ets:tab()) -> ok.
%% ---------------------------------------------
on_expire(Args, EtsMap, EtsHash) ->
    update_map(Args, EtsMap, EtsHash).

%% ---------------------------------------------
-spec on_delete(Args :: list(), EtsMap :: ets:tab(), EtsHash :: ets:tab()) -> ok.
%% ---------------------------------------------
on_delete(Args, EtsMap, EtsHash) ->
    update_map(Args, EtsMap, EtsHash).

%% ====================================================================
%% Internal functions
%% ====================================================================

update_map([Key, Map], EtsMap, EtsHash) ->
    update_map([Key, Map, 0], EtsMap, EtsHash);
%%
update_map([Key, Map, 0], EtsMap, EtsHash) ->
    EventList = make_event_list(Map),
    ets:delete(EtsHash,Key),
    lists:foreach(fun(El) -> delete_record(El, Key, EtsMap) end, EventList);
%%
update_map([Key, Map, _Ttl], EtsMap, EtsHash) ->
    EventList = make_event_list(Map),
    case check_event_hashs(Key,Map,EventList,EtsHash) of
        not_changed -> {ok, Key};
        {changed, DelEventList} ->
            ets:delete(EtsHash,Key),
            lists:foreach(fun(El) -> delete_record(El, Key, EtsMap) end, DelEventList),
            lists:foreach(fun(El) -> update_record(El, Key, EtsMap) end, EventList),
            ets:insert(EtsHash, #event_hash{key=Key,value=Map,hash=erlang:phash2(Map)}),
            {ok, Key}
    end.

%% @private
check_event_hashs(Key,Map,NewEventList,EtsHash) ->
    MapPhash = erlang:phash2(Map),
    case ets:lookup(EtsHash, Key) of
        [] -> {changed,[]};
        [#event_hash{hash=MapPhash}] -> not_changed;
        [#event_hash{value=OldMap}] ->
            OldEventList = make_event_list(OldMap),
            DelEventList = OldEventList -- NewEventList,
            ets:delete(EtsHash,Key),
            {changed, DelEventList}
    end.

%% @private
make_event_list(Map) ->
    [Objs, Events] = ?BU:maps_get([<<"objects">>, <<"events">>], Map),
    [{Ev,O} || O <- Objs, Ev <- Events].

%% @private
delete_record(El, Key, EtsMap) ->
    case ets:lookup(EtsMap, El) of
        [#event_map{list=[Key]}] -> ets:delete(EtsMap, El);
        [#event_map{list=[_]}] -> ok;
        [#event_map{list=L}] -> ets:update_element(EtsMap, El, {#event_map.list,lists:delete(Key, L)});
        [] -> ok
    end.

%% @private
update_record(El, Key, EtsMap) ->
    case ets:lookup(EtsMap, El) of
        [#event_map{list=[Key]}] -> ok;
        [#event_map{list=L}] ->
            case lists:member(Key, L) of
                true -> ok;
                false -> ets:update_element(EtsMap, El, {#event_map.list,[Key|L]})
            end;
        [] -> ets:insert(EtsMap, #event_map{key=El, list=[Key]})
    end.
