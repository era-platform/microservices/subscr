%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Domain subscription application service server
%%%     Opts:
%%%         domain
%%%         ets_data
%%%         ets_map
%%%         ets_hash
%%%         sync_ref

-module(subscr_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(dstate, {
    domain :: binary() | atom(),
    sync_ref :: reference(),
    %
    subscrid :: binary(),
    %
    ref :: reference(),
    timerref :: reference(),
    %
    ets_data :: ets:tab(),
    ets_map :: ets:tab(),
    ets_hash :: ets:tab(),
    %
    is_paused = false :: boolean(),
    pause_reason :: term(),
    %
    stored_events = [] :: list(),
    %
    search_pid :: pid(),
    search_monref :: reference(),
    modify_pid :: pid(),
    modify_monref :: reference(),
    %
    start_ts :: non_neg_integer(),
    loaded_ts :: non_neg_integer()
}).

-define(TimeoutInit, 10000).
-define(TimeoutRegular, 60000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    Domain = ?BU:get_by_key('domain',Opts),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    ?GLOBAL:gen_server_start_link({gnr,multi,GlobalName}, ?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    SyncRef = ?BU:get_by_key(sync_ref,Opts),
    Self = self(),
    Ref = make_ref(),
    State = #dstate{domain = Domain,
                    sync_ref = SyncRef,
                    subscrid = SubscrId=?BU:strbin("sid_SUBSCR:~ts;~ts",[node(),Domain]),
                    ref = Ref,
                    start_ts = ?BU:timestamp(),
                    ets_data = ?BU:get_by_key(ets_data,Opts),
                    ets_map = ?BU:get_by_key(ets_map,Opts),
                    ets_hash = ?BU:get_by_key(ets_hash,Opts)},
    % ----
    ?BLstore:store_u({?APP,sync,Domain},{'facade',SyncRef,self()}),
    Self ! {'init',Ref},
    % ----
    SubscrOpts = #{<<"ttl_first">> => 20,
                   fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?SubscriptionsCN,self(),SubscrId,SubscrOpts),
    % ----
    ?LOG('$info', "~ts. '~ts' srv inited", [?APP,Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% Debug restart
handle_call({restart}, _From, State) ->
    {stop,State};

%% --------------
%% Search
handle_call(_, _From, #dstate{is_paused=true,pause_reason=PauseReason}=State) ->
    {reply,PauseReason,State};
%%
handle_call({search,_}, _From, #dstate{search_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    Reply = {retry_after,RetryTimeout,<<"Search-srv process is not ready yet">>},
    {reply,Reply,State};
%%
handle_call({search,Args}, From, #dstate{search_pid=Pid}=State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    gen_server:cast(Pid, {search,Args,FunReply}),
    {noreply,State};

%% -------------
%% Put subscription (also if TTL=0)
handle_call({put,Args}, From, State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    State1 = put(Args,FunReply,State),
    {noreply,State1};

%% -------------
%% Delete subscription
handle_call({del,Args}, From, State) ->
    FunReply = fun(Reply) -> gen_server:reply(From,Reply) end,
    State1 = del(Args,FunReply,State),
    {noreply,State1};

%% --------------
%% Other
handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% -------------
%% Put subscription (also if TTL=0)
handle_cast({put,Args}, State) ->
    State1 = put(Args,fun(_) -> ok end,State),
    {noreply,State1};

%% -------------
%% Delete subscription
handle_cast({del,Args}, State) ->
    State1 = del(Args,fun(_) -> ok end,State),
    {noreply,State1};

%% -------------
%% Other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% init
handle_info({init,Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',SyncRef,Report}, #dstate{sync_ref=SyncRef,timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    State1 = case Report of
                 ok ->
                     Timeout = case ?BU:timestamp() of
                                   NowTS when LoadedTS/=undefined, NowTS - LoadedTS < 5000 -> LoadedTS + 5000 - NowTS;
                                   _ -> 0
                               end,
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#dstate{ref=Ref1,
                                  timerref = erlang:send_after(Timeout, self(), {timer,init,Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'init',Ref}, #dstate{ref=Ref}=State) ->
    State1 = init_data(State),
    {noreply, State1};

%% --------------
%% Init timer expired
handle_info({timer,'regular',Ref}, #dstate{ref=Ref}=State) ->
    Ref1 = make_ref(),
    State1 =  State#dstate{ref=Ref1,
                           timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
    {noreply, State1};

%% --------------
%% On register of search srv. Skip if duplicate.
handle_info({register_srv,?DSearchSrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,search_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DSearchSrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' search-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{search_pid=Pid,
                           search_monref=MonRef}};

%% --------------
%% On register of modify srv. Skip if duplicate.
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,modify_pid=Pid}=State) -> {noreply,State};
handle_info({register_srv,?DModifySrv,SyncRef,Pid}, #dstate{sync_ref=SyncRef,domain=Domain}=State) ->
    ?LOG('$trace',"~ts. '~ts' modify-srv registered: ~p",[?APP,Domain,Pid]),
    MonRef = erlang:monitor(process,Pid),
    {noreply, State#dstate{modify_pid=Pid,
                           modify_monref=MonRef}};

%% --------------
%% DOWN search srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{search_monref=MonRef,search_pid=Pid}=State) ->
    {noreply, State#dstate{search_pid=undefined,
                           search_monref=undefined}};

%% --------------
%% DOWN modify srv
handle_info({'DOWN',MonRef,process,Pid,_Reason}, #dstate{modify_monref=MonRef,modify_pid=Pid}=State) ->
    {noreply, State#dstate{modify_pid=undefined,
                           modify_monref=undefined}};

%% --------------
%% On notify from DMS about changes in subscriptions
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId,is_paused=true,stored_events=StoredEvents}=State) ->
    State1 = State#dstate{stored_events=[EventInfo|StoredEvents]},
    {noreply, State1};
%%
handle_info({notify,SubscrId,EventInfo}, #dstate{subscrid=SubscrId}=State) ->
    State1 = apply_changed_event(EventInfo, State),
    {noreply, State1};

%% --------------
%% CRUD operation
handle_info({'$call', {FromPid,Ref}, {crud,?SubscriptionsCN,{Operation,Map}}=CrudReq}, #dstate{domain=Domain}=State) ->
    FunReply = fun(Reply) -> FromPid ! {'$reply',Ref,Reply} end,
    _Object = maps:get('object',Map),
    State1 = case Operation of
                 'read' ->
                     spawn(fun() ->
                         GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
                         Res = (catch ?GLOBAL:gen_server_call(GlobalName, CrudReq, 30000)),
                         FunReply(Res)
                           end),
                     State;
                 _ -> FunReply({error,{405,<<"Direct access to collection not allowed">>}}), State
             end,
    {noreply,State1};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------
%% initialization of data. Load all subscriptions from DMS.
%% ------------------------------------------
init_data(#dstate{}=State) ->
    State1 = State#dstate{stored_events=[]},
    Ref1 = make_ref(),
    case catch load(State1) of
        {ok,State2} ->
            State3 = State2#dstate{is_paused = false,
                                   pause_reason = undefined,
                                   loaded_ts = ?BU:timestamp(),
                                   ref=Ref1,
                                   timerref = erlang:send_after(?TimeoutRegular, self(), {timer,regular,Ref1})},
            apply_changed_events(State3);
        {error,_}=Err ->
            State1#dstate{is_paused = true,
                          pause_reason = Err,
                          ref=Ref1,
                          timerref = erlang:send_after(?TimeoutInit, self(), {timer,init,Ref1})}
    end.

%% @private
load(#dstate{domain=Domain,ets_data=EtsData}=State) ->
    ets:delete_all_objects(EtsData),
    ok = ?DMS_CACHE:read_all_by_portions(Domain,?SubscriptionsCN,{<<"timestamp">>,<<"desc">>},[],10000,EtsData),
    ets:foldl(fun({Id,Item},_) -> ets:insert(EtsData,{Id,shrink(Item)}) end, ok, EtsData),
    build_hashes(State),
    {ok,State}.

%% @private
build_hashes(#dstate{ets_data=EtsData,ets_map=EtsMap,ets_hash=EtsHash}) ->
    ets:delete_all_objects(EtsMap),
    ets:delete_all_objects(EtsHash),
    ets:foldl(fun({_Id,Item},ok) ->
                    SID = maps:get(<<"sid">>,Item),
                    ItemShrink = shrink(Item),
                    case maps:get(<<"expires">>,Item,0) of
                        Expires when Expires>0 -> ?DModifyUtils:on_put([SID,ItemShrink,Expires],EtsMap, EtsHash);
                        _ -> ?DModifyUtils:on_delete([SID,ItemShrink],EtsMap, EtsHash)
                    end,
                    ok
              end, ok, EtsData).

%% @private
shrink(Item) -> maps:without([<<"expires">>,<<"timestamp">>],Item).

%% ------------------------------------------
%% Applying of events about changes in subscriptions
%% ------------------------------------------
apply_changed_events(#dstate{stored_events=StoredEvents}=State) ->
    State1 = lists:foldr(fun(EventInfo,StateX) -> apply_changed_event(EventInfo,StateX) end, State, StoredEvents),
    State1#dstate{stored_events=[]}.

%% Apply one event
apply_changed_event(EventInfo, #dstate{domain=Domain,ets_data=EtsData,ets_map=EtsMap,ets_hash=EtsHash,timerref=TimerRef}=State) ->
    Data = maps:get(<<"data">>,EventInfo),
    case maps:get(<<"operation">>,Data) of
        'clear' ->
            ets:delete_all_objects(EtsData),
            ets:delete_all_objects(EtsMap),
            ets:delete_all_objects(EtsHash),
            State;
        'reload' ->
            ?BU:cancel_timer(TimerRef),
            Ref1 = make_ref(),
            State#dstate{ref=Ref1,
                         timerref = erlang:send_after(?BU:random(10000), self(), {timer,init,Ref1})};
        O ->
            Item = maps:get(<<"entity">>,Data),
            Id = maps:get(<<"id">>,Item),
            Key = maps:get(<<"sid">>,Item),
            TTL = maps:get(<<"expires">>,Item),
            case O of
                'create' ->
                    ItemShrink = shrink(Item),
                    ets:insert(EtsData,{Id,ItemShrink}),
                    ?DModifyUtils:on_put([Key,ItemShrink,TTL], EtsMap, EtsHash),
                    State;
                'update' ->
                    ItemShrink = shrink(Item),
                    ets:insert(EtsData,{Id,ItemShrink}),
                    ?DModifyUtils:on_put([Key,ItemShrink,TTL], EtsMap, EtsHash),
                    State;
                'delete' ->
                    ets:delete(EtsData,Id),
                    ItemShrink = shrink(Item),
                    ?DModifyUtils:on_delete([Key,ItemShrink], EtsMap, EtsHash),
                    State;
                'corrupt' ->
                    spawn(fun() -> on_corrupt(Domain,Item,{EtsData,EtsMap,EtsHash}) end),
                    State
            end
    end.

%% @private
%% Async process
on_corrupt(Domain,Item,{EtsData,EtsMap,EtsHash}) ->
    Id = maps:get(<<"id">>,Item),
    Key = maps:get(<<"sid">>,Item),
    TTL = maps:get(<<"expires">>,Item),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Args = {'read',#{class => ?SubscriptionsCN,
                     object => {'entity',Id,[]},
                     qs => #{}}},
    case catch ?GLOBAL:gen_server_call(GlobalName,{crud,?SubscriptionsCN,Args}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Call to DMS (read subscription '~ts', id='~ts', sid='~ts') crashed: ~n\t~120tp",[Domain,Id,Key,Reason]),
            ok;
        {error,_}=Err ->
            ?LOG('$error',"Call to DMS (read subscription '~ts', id='~ts', sid='~ts') error: ~n\t~120tp",[Domain,Id,Key,Err]),
            ets:delete(EtsData,Id),
            ItemShrink = shrink(Item),
            ?DModifyUtils:on_delete([Key,ItemShrink], EtsMap, EtsHash);
        {ok,Item} ->
            ItemShrink = shrink(Item),
            ets:insert(EtsData,{Id,ItemShrink}),
            ?DModifyUtils:on_put([Key,ItemShrink,TTL], EtsMap, EtsHash)
    end.

%% ------------------------------------------
%% Put
%% ------------------------------------------
put([SubscrId,Args,TTL],FunReply,#dstate{modify_pid=undefined,subscrid=SubscrId,ets_map=EtsMap,ets_hash=EtsHash}=State) ->
    % HERE IS START LOAD POINT OF DOMAIN
    % To avoid of period without notifications after subscriptions loaded
    ItemShrink = ?DModifySrv:build_entity([SubscrId,Args,TTL]),
    ?DModifyUtils:on_put([SubscrId,ItemShrink,TTL], EtsMap, EtsHash),
    % result is common retry, not success. Helper should retry in few moments to ensure subscription to DMS
    RetryTimeout = retry_timeout(State),
    FunReply({retry_after,RetryTimeout,<<"Modify-srv process is not ready yet">>}),
    State;
%%
put(_,FunReply,#dstate{modify_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    FunReply({retry_after,RetryTimeout,<<"Modify-srv process is not ready yet">>}),
    State;
%%
put([_|_]=Args,FunReply,#dstate{modify_pid=Pid}=State) ->
    gen_server:cast(Pid, {put,Args,FunReply}),
    State.

%% ------------------------------------------
%% Delete
%% ------------------------------------------
%%
del(_,FunReply,#dstate{modify_pid=undefined}=State) ->
    RetryTimeout = retry_timeout(State),
    FunReply({retry_after,RetryTimeout,<<"Modify-srv process is not ready yet">>}),
    State;
%%
del([_|_]=Args,FunReply,#dstate{modify_pid=Pid}=State) ->
    gen_server:cast(Pid, {del,Args,FunReply}),
    State.

%% @private
retry_timeout(#dstate{start_ts=StartTS}) ->
    Diff = ?BU:timestamp() - StartTS,
    erlang:min(5000,erlang:max(20,Diff)).
