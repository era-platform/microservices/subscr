%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Validates 'subscription' entity on any modify operation.

-module(subscr_validator_coll_subscriptions).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=_EntityInfo,_ModifierInfo) ->
    FunIsFixt = fun(_E) -> false end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        _ when Operation=='delete' ->
            {ok,undefined};
        _ ->
            case check_entity(Domain,Operation,E1) of
                {error,_}=Err -> Err;
                {ok,EntityV} -> {ok,EntityV}
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check properties
%% -------------------------------------
check_entity(Domain,Operation,Item) ->
    check_x(Domain,Operation,Item).

%% final
check_x(_Domain,_Operation,Item) -> {ok,Item}.